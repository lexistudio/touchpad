define([
	'jquery',
	'site/config'
], function ($, config) {

	"use strict";

		var v = {
			text: '',
			desc: ''
		};

		var style = {
			backCss: {
				position: 'absolute',
				left: 0,
				top: 0,
				width: '100%',
				height: '100%',
				background: 'rgba(237, 242, 248, .7)',
				opacity: '0',
				zIndex: 9999, /* replace bugfix old val 999999 http://joxi.ru/brRkGYZHJq5pPm */
				textAlign: 'center',
				overflow: 'hidden',
				font: '17px/normal "PT Sans", sans-serif',
				color: '#ffffff'
			},
			textCss: {
				// display: 'none',
				color: '#ffffff',
				position: 'absolute',
				width: '100%'
			},
			iconCss: {
				display: 'inline',
				width: '50px'
			}
		};

		var m = {
			init: function (arg) {
				$.extend(v, arg);
				// if (this.css('position') != 'relative') this.css('position', 'relative');
				var contentTop = s.getTop(this);
				if (this.find('.preload-content').length == 0) {
					var back = $('<div class="preload-content"><div class="preload-content-text" style="top: ' + contentTop + 'px;"><span style="font-weight: normal; font-size: 21px; margin-top: 60px;">' + v.text + '</span><br />' + v.desc + '</div><img src="' + config.frontendPath + '/assets/image/preloader-oval.svg" style="margin-top: ' + parseInt(contentTop) + 'px;" /></div>');
					back.css(style.backCss);
					back.find('.preload-content-text').css(style.textCss);
					back.find('img').css(style.iconCss);
					this.append(back);
					back.animate({opacity: 1}, 300, function () {
					});
				}

				/*this.resize(function(){
					contentTop = s.getTop(this);
					console.log(contentTop);
					back.find('div:first').css({top: contentTop});
					back.find('img:first').css({marginTop: (parseInt(contentTop)+90)});
				});*/

			},

			remove: function () {
				var p = this.find('.preload-content');
				p.stop().animate({opacity: 0}, 200, function () {
					p.remove();
				});
			}
		};

		var s = {
			getTop: function (wis) {
				var height = parseInt(wis.css('height')) + parseInt(wis.css('padding-top')) + parseInt(wis.css('padding-bottom'));
				return (parseInt(height) / 2) - 25;
			}
		};

		$.fn.preload = function (method) {

			if (m[method]) {
				return m[method].apply(this, Array.prototype.slice.call(arguments, 1));
			} else if (typeof method === "object" || !method) {
				return m.init.apply(this, arguments);
			} else {
				$.error("Method " + method + " does not exist");
			}
		};

});
