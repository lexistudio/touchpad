define([
  'jquery'
], function($) {
  "use strict";

  $(function () {

    // Клавиатура
    (function () {
      var _el        = $(".js-keyboard");
      var _keyboard = $(".keyboard__button");

      _el.each(function (i, e) {
        var _e = $(e);

        if ( _e.data("key") == "on" ) {
          _e.focus();
        }
      });

      _el.on("mousedown touchstart", function () {
        _el.attr("data-key", "off");
        $(this).attr("data-key", "on");

        if ( $(this).attr("id") === "date" ) {
          _keyboard.not(".keyboard__button--number").addClass("disabled");
        }
        else {
          _keyboard.removeClass("disabled");
        }
      });

      _keyboard.on("mousedown touchstart", function (e) {
        e.preventDefault();

        var _key = $(this).data("key");

        _el.each(function (i, e) {
          var _e = $(e);

          if ( _e.attr("data-key") === "on" ) {
            _e.focus();

            if ( _e.attr("id") === "date" ) {
              if ( _e.val().length > 4 ) {
                console.log(false);
              }
            }

            if ( _e.val().length >= 0 ) {
              _e.removeClass("error");
            }

            if ( _key === "#clear" ) {
              _e.val('');
            }
            else if ( _key === "#back" ) {
              _e.val( _e.val().substring(0, _e.val().length - 1) );
            }
            else {
              _e.val( _e.val() + _key );
            }
          }
        });
      });
    })();

  });

});