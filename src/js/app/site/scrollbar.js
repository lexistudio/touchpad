define([
  'jquery',
  'jquery/scrollbar'
], function($, SimpleBar) {
  "use strict";

  $(function () {

    // Кастомный scrollbar
    (function () {
      // В карточке
      var el       = new SimpleBar(document.querySelector('[data-simplebar]'));

      // В результатах поиска
      var elResult = new SimpleBar(document.querySelector('[data-simplebar-result]'));
    })();

  });

});