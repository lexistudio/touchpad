define([
  'jquery',
  'jquery/mask'
], function($) {
  "use strict";

  $(function () {

    // Настройки для браузера по умолчанию
    (function () {
      // Открытия браузера во весь экран
      // Может вызывать ошибку
      // При необходимости можно отключить
      var elem = document.documentElement;

      if (elem.requestFullscreen) {
        elem.requestFullscreen()
      }

      // Отключение контестного меню по клюку правой кнопкой миши
      $(document).on("contextmenu", function (e) {
        e.preventDefault();
      });
    })();

    // Настройка для поля дата
    (function () {
      var _el = $("#date");

      _el.mask("rrrrrrrrrr", {
        translation: {
          r: {
            pattern: /[0-9.]/,
            optional: true
          }
        }
      });
    })();

  });

});