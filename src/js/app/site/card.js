define([
  'jquery',
  'jquery/qrcode'
], function($) {
  "use strict";

  $(function () {

    // Карточка
    (function () {
      $(document).on("click", ".js-list", function (e) {
        e.preventDefault();
        $("#two").addClass("hide");
        $("#three").addClass("active");
        var _this = $(this);

        var _arr = [
          {"title" : "Год/Дата рождения", "txt" : _this.data("birthday")},
          {"title" : "Место рождения", "txt" : _this.data("birthplace")},
          {"title" : "Место призыва", "txt" : _this.data("callplace")},
          {"title" : "Дата призыва", "txt" : _this.data("calldate")},
          {"title" : "Воинское звание", "txt" : _this.data("rank")},
          {"title" : "Место службы", "txt" : _this.data("unit")}
        ];

        $("#cardList").html('');

        for ( var i in _arr ) {
          var _arg = _arr[i];

          if ( String(_arg.txt).length === 0 ) continue;

          var _wrap = $("<div/>", {
            "class" : "card__right-list-item"
          }).appendTo( $("#cardList") );

          $("<div/>", {
            "class" : "card__right-list-title",
            "html" : _arg.title
          }).appendTo(_wrap);

          $("<div/>", {
            "class" : "card__right-list-txt",
            "html" : _arg.txt
          }).appendTo(_wrap);
        }

        var _qcode = new QRCode("qcode", {
          text: "https://foto.pamyat-naroda.ru/detail/" + $(this).data("id"),
          width: 100,
          height: 100,
          colorDark : "#000",
          colorLight : "#fff",
          correctLevel : QRCode.CorrectLevel.H
        });

        $(".js-card-photo").css("backgroundImage", "url(" + $(this).data("photo") + ")");
        $(".js-list-card").css("backgroundImage", "url(" + $(this).data("photo") + ")");
        $(".js-list-card").attr("data-url", $(this).data("photo"));
        $(".card__right-name").html("<span>" + $(this).data("last") + "</span>" + $(this).data("name"));
      });
    })();

    (function () {
      var _btn = $(".js-close");

      if (_btn.length) {
        _btn.on("click", function (e) {
          e.preventDefault();
          $("#two").removeClass("hide");
          $("#three").removeClass("active");
        });
      }
    })();

    (function () {
      var _btn   = $(".js-list-card");
      var _photo = $(".js-card-photo");

      if (_btn.length) {
        _btn.on("click", function (e) {
          e.preventDefault();
          var _href = $(this).data("url");

          _photo.css("backgroundImage", "url(" + _href + ")");

          _btn.removeClass("active");
          $(this).addClass("active");
        });
      }
    })();

  });

});