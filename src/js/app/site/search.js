define([
  'jquery'
], function($) {
  "use strict";

  $(function () {

    // Форма поиска
    (function () {
      var _btn = $(".js-search-button");

      if (_btn.length) {
        _btn.on("submit", function (e) {
          e.preventDefault();
          var _lastname   = $("#lastname"),
              _firstname  = $("#firstname"),
              _patronymic = $("#patronymic"),
              _date       = $("#date"),
              _rank       = $("#rank");

          if (_lastname.val().length != 0) {
            _lastname.removeClass("error");

            $("#one, #two").addClass("active");
            $("html, body").animate({
              scrollTop: $("#two").offset().top
            });

            var FIRSTNAME  = _firstname.val() != 0 ? ", " + _firstname.val() : "",
                PATRONYMIC = _patronymic.val() != 0 ? ", " + _patronymic.val() : "",
                DATE       = _date.val() != 0 ? ", " + _date.val() : "",
                RANK       = _rank.val() != 0 ? ", " + _rank.val() : "";

            $("#filter").find("span").text(
              _lastname.val() +
              FIRSTNAME +
              PATRONYMIC +
              DATE +
              RANK
            );

            $("#list").html("");

            var _lastnameRegExp   = new RegExp(_lastname.val(), "i"),
                _firstnameRegExp  = new RegExp(_firstname.val(), "i"),
                _patronymicRegExp = new RegExp(_patronymic.val(), "i"),
                _dateRegExp       = new RegExp(_date.val(), "i"),
                _rankRegExp       = new RegExp(_rank.val(), "i");

            $.getJSON("/db.json", function (response) {
              $("#list").addClass("none-padding");

              $.each(response.items, function (i, e) {
                var _lastnameNullReset   = e.Lastname != null ? e.Lastname : '',
                    _firstnameNullReset  = e.Firstname != null ? e.Firstname : '',
                    _patronymicNullReset = e.Patronymic != null ? e.Patronymic : '',
                    _dateNullReset       = e.Birthday != null ? e.Birthday : '',
                    _rankNullReset       = e.Rank != null ? e.Rank : '',
                    _photoNullReset      = e.MainPhoto != null ? e.MainPhoto : '',
                    _birthplaceNullReset = e.Birthplace != null ? e.Birthplace : '',
                    _callplaceNullReset  = e.Callplace != null ? e.Callplace : '',
                    _calldateNullReset   = e.Calldate != null ? e.Calldate : '',
                    _unitNullReset       = e.Unit != null ? e.Unit : '';

                if (
                  _lastnameNullReset.search(_lastnameRegExp) != -1 &&
                  _firstnameNullReset.search(_firstnameRegExp) != -1 &&
                  _patronymicNullReset.search(_patronymicRegExp) != -1 &&
                  _dateNullReset.search(_dateRegExp) != -1 &&
                  _rankNullReset.search(_rankRegExp) != -1
                ) {

                  console.log( e.Lastname + "  " + e.Firstname + " " + e.Patronymic + " " + e.Birthday + " " + e.Rank );
                  console.log( " --- " );

                  var _wrap = $("<div/>", {
                     "class" : "list__item js-list",
                     "data-photo" : _photoNullReset,
                     "data-name" : _firstnameNullReset + " " + _patronymicNullReset,
                     "data-last" : _lastnameNullReset,
                     "data-birthday" : _dateNullReset,
                     "data-birthplace" : _birthplaceNullReset,
                     "data-callplace" : _callplaceNullReset,
                     "data-calldate" : _calldateNullReset,
                     "data-rank" : _rankNullReset,
                     "data-unit" : _unitNullReset,
                     "data-id" : e.Id,
                  }).appendTo( $("#list") );

                  var _wrapContent = $("<div/>", {
                    "class" : "list__content"
                  }).appendTo(_wrap);

                  $("<div/>", {
                    "class" : "list__content-photo",
                    "style" : "background-image: url(" + _photoNullReset + ")"
                  }).appendTo(_wrapContent);

                  var _wrapTxt = $("<div/>", {
                    "class" : "list__content-txt",
                    "html": "<span>" + _lastnameNullReset + "</span> " + _firstnameNullReset + " " + _patronymicNullReset
                  }).appendTo(_wrapContent);
                }
              });
            });
          }
          else {
            _lastname.addClass("error");
          }
        });

        $(".js-reset").on("click", function (e) {
          e.preventDefault();

          $("#one, #two, #three").removeClass("active");
          $("#two").removeClass("hide");

          $("html, body").animate({
            scrollTop: 0
          });

          $("#filter").find("span").text('');

          $("#lastname, #firstname, #patronymic, #date, #rank").val('');
        });

        $(".js-update").on("click", function (e) {
          e.preventDefault();

          $("#one, #two, #three").removeClass("active");
          $("#two").removeClass("hide");

          $("html, body").animate({
            scrollTop: 0
          });
        });
      }
    })();

  });

});